#!/bin/bash

docker run \
  -it \
  --rm \
  --name node-carbon-angular6 \
  -v `pwd`:/home/node/dev \
  -p 4200:4200 \
  -p 9876:9876 \
  avejidah/node-carbon-angular6 \
  /bin/bash

