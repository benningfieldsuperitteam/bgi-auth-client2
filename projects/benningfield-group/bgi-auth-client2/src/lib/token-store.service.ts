import { Injectable } from '@angular/core';

import { IDTokenType } from './id-token.type';
import { AccessTokenType } from './access-token.type';
import { TokenService } from './token.service';

/**
 * In-memory storage for the access and id tokens.
 */
@Injectable({
  providedIn: 'root'
})
export class TokenStore {
  private idToken: string;
  private accessToken: string;
  private idTokenPayload: IDTokenType;
  private accessTokenPayload: AccessTokenType;

  /**
   * Initialize with the tokens and payloads set to null.
   */
  constructor(
    private tokenSvc: TokenService) {
    this.clear();
  }

  /**
   * Clear the tokens and payloads.
   */
  clear(): void {
    this.idToken            = null;
    this.accessToken        = null;
    this.idTokenPayload     = null;
    this.accessTokenPayload = null;
  }

  /**
   * Store the tokens and payloads.
   */
  store(idToken: string, accessToken: string) {
    this.idToken            = idToken;
    this.accessToken        = accessToken;
    this.idTokenPayload     = this.tokenSvc.decode(idToken);
    this.accessTokenPayload = this.tokenSvc.decode(accessToken);
  }

  /**
   * Get the access token.
   */
  getAccessToken(): string {
    return this.accessToken;
  }

  /**
   * Get the id token.
   */
  getIDToken(): string {
    return this.idToken;
  }

  /**
   * Get the access token payload.
   */
  getAccessTokenPayload(): AccessTokenType {
    return this.accessTokenPayload;
  }

  /**
   * Get the id token payload.
   */
  getIDTokenPayload(): IDTokenType {
    return this.idTokenPayload;
  }
}

