export class AuthConfig {
  clientID: string;
  redirectURI: string;
  postLogoutRedirectURI: string;
  authEndpoint: string;
}

