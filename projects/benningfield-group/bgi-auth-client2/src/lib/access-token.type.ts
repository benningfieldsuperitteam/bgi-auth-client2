export class AccessTokenType {
  iat: number;
  exp: number;
  aud: string[];
  iss: string;
  sub: string;
  jti: string;
  roles: string[];
}

