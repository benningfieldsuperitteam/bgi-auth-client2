import { TokenStore } from './token-store.service';
import { TokenService } from './token.service';

describe('TokenStore', () => {
  let tokenStore: TokenStore;
  let idToken: string;
  let accessToken: string;

  beforeEach(() => {
    tokenStore = new TokenStore(new TokenService());
    idToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3OD' +
      'kwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJub25jZSI6IjBjMTU' +
      'yZGMwMjBiZjU3NTA1OGExZjg1ZDMyODFhOGIyMjE0Y2Y1NjU4ZjIwODM4MGRjZDcwZmI1' +
      'YjIxZjllYjYifQ.ApZGVwqdJ8M8WRrKH6VCS3wJaZe28zMfzl5q7i2NA8o';
    accessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3O' +
      'DkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJhdWQiOlsiZmFrZS1' +
      'hdWQiXX0.untZbXZsokqfuLg4rV22GBV8AGdjduZRaqeoNSpdoEY';
  });

  describe('.constructor()', () => {
    it('initializes with null tokens and payloads.', () => {
      expect(tokenStore.getAccessToken()).toBeNull();
      expect(tokenStore.getIDToken()).toBeNull();
      expect(tokenStore.getAccessTokenPayload()).toBeNull();
      expect(tokenStore.getIDTokenPayload()).toBeNull();
    });
  });

  describe('.store()', () => {
    it('stores the tokens and decoded payloads.', () => {
      tokenStore.store(idToken, accessToken);

      expect(tokenStore.getIDToken()).toBe(idToken);
      expect(tokenStore.getAccessToken()).toBe(accessToken);
      expect(tokenStore.getIDTokenPayload().sub).toBe('1234567890');
      expect(tokenStore.getAccessTokenPayload().aud).toEqual(['fake-aud']);
    });
  });

  describe('.clear()', () => {
    it('clears the storage.', () => {
      tokenStore.store(idToken, accessToken);
      tokenStore.clear();

      expect(tokenStore.getAccessToken()).toBeNull();
      expect(tokenStore.getIDToken()).toBeNull();
      expect(tokenStore.getAccessTokenPayload()).toBeNull();
      expect(tokenStore.getIDTokenPayload()).toBeNull();
    });
  });
});
