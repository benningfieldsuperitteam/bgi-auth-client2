import { Injectable, Inject } from '@angular/core';

import { sha256 } from 'js-sha256';

import {
  RandomService, StorageService, URLService, WindowRef
} from '@benningfield-group/bgi2';

import { AuthConfig } from './auth.config';
import { TokenService } from './token.service';
import { IDTokenType } from './id-token.type';
import { AccessTokenType } from './access-token.type';
import { TokenStore } from './token-store.service';

const STATE_NAME = 'bgi-auth:state';
const NONCE_NAME = 'bgi-auth:nonce';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    @Inject('authConfig') private authConfig: AuthConfig,
    private randSvc: RandomService,
    private storageSvc: StorageService,
    private urlSvc: URLService,
    private windowRef: WindowRef,
    private tokenSvc: TokenService,
    private tokenStore: TokenStore) {
  }

  /**
   * Build the authorize URI.  This implies setting a nonce and state in local
   * storage, since they're required to be present in the URI.
   */
  buildAuthURI(scope: string = 'openid', prompt?: string, responseMode?: string): string {
    const params  = [];
    const nonce   = this.randSvc.generateRandomString(30);
    const state   = this.randSvc.generateRandomString(30);
    let   authURI = `${this.authConfig.authEndpoint}/api/authorize?`;

    // "state" and "nonce" get saved in local storage so that they can be
    // verified when the auth server sends the user back.
    //
    // The nonce is used to prevent replay attacks.  If a redirect back
    // from the auth server is comprimised, an attacker would not be able
    // to blindly use it without the clear-text nonce (it's sent to the
    // auth server hashed).
    this.storageSvc.setObject(NONCE_NAME, nonce);
    this.storageSvc.setObject(STATE_NAME, state);

    params.push('scope='         + encodeURIComponent(scope));
    params.push('response_type=' + encodeURIComponent('id_token token'));
    params.push('client_id='     + encodeURIComponent(this.authConfig.clientID));
    params.push('redirect_uri='  + encodeURIComponent(this.authConfig.redirectURI));
    params.push('nonce='         + encodeURIComponent(sha256(nonce)));
    params.push('state='         + encodeURIComponent(state));

    if (prompt)
      params.push('prompt=' + encodeURIComponent(prompt));

    if (responseMode)
      params.push('response_mode=' + encodeURIComponent(responseMode));

    authURI += params.join('&');

    return authURI;
  }

  /**
   * Go to the auth endpoint.
   */
  authenticate(scope?: string) {
    this.urlSvc.go(this.buildAuthURI(scope));
  }

  /**
   * Tries to authenticate silently using an iframe and no prompt.  This can be
   * used to auto-login (e.g. if the user is already logged in to the auth
   * server but not the local app), or to renew an auth token.
   */
  authenticateSilently(scope?: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const self = this;

      // The auth request is made in an iframe using the "web_message" response
      // mode (https://tools.ietf.org/html/draft-sakimura-oauth-wmrm-00).
      const iframe = this.windowRef.nativeWindow.document.createElement('iframe');

      // The auth endpoint sends the response (the id_token and such) using the
      // HTML5 messaging API.
      this.windowRef.nativeWindow.addEventListener('message', onAuthMessage);

      iframe.style.display = 'none';
      iframe.src = this.buildAuthURI(scope, 'none', 'web_message');
      iframe.id = 'bgiAuthIframe';
      this.windowRef.nativeWindow.document.body.appendChild(iframe);

      function onAuthMessage(e) {
        // Only accept messages from the auth server.  (The application that's
        // using this library may be using the messaging API for other reasons,
        // so a different origin is not necessarily an error).
        if (e.origin !== self.authConfig.authEndpoint)
          return;

        // Clean up the iframe and event listener.
        iframe.remove();
        self.windowRef.nativeWindow.removeEventListener('message', onAuthMessage);

        try {
          self.handleAuthWebMessage(e);
          resolve();
        }
        catch (e) {
          reject(e);
        }
      }
    });
  }

  /**
   * Handles posted messages from the auth server in web message resonse mode.
   */
  handleAuthWebMessage(e: MessageEvent): void {
    if (!e.data)
      throw new Error('AuthService:authenticateSilently No message data received in authorization message.');

    if (e.data.type !== 'authorization_response')
      throw new Error(`AuthService:authenticateSilently Invalid authorization response type: "${e.data.type}"`);

    const response = e.data.response;

    if (!response)
      throw new Error('AuthService:authenticateSilently Missing "response" in authorization message.');

    // Errors from the auth endpoint are returned unmodified to the client
    // application.  "login_required" is the standard OIDC message that's
    // sent back from the auth server if the user is not logged in.
    if (response.error)
      throw new Error(response.error);

    for (const key of ['state', 'id_token', 'token']) {
      if (!response[key])
        throw new Error(`AuthService:authenticateSilently Missing "${key}" in authorization message response.`);
    }

    this.handleAuthResponse(response.state, response.id_token, response.token);
  }

  /**
   * Handle an auth redirect response from the server.
   */
  handleAuthRedirect(): void {
    if (!this.authenticating())
      throw new Error('AuthService:handleAuthRedirect Authentication not in progress.');

    // Make a map out of the fragment parts.
    const hashParams = this.urlSvc.getFragmentParams();

    // Clear the fragment as it contains the token and is no longer needed.
    this.urlSvc.setFragment();

    if (hashParams.has('error'))
      throw new Error(hashParams.get('error'));

    this.handleAuthResponse(hashParams.get('state'),
      hashParams.get('id_token'), hashParams.get('token'));
  }

  /**
   * Helper method that checks the auth response and sets up the token storage.
   */
  protected handleAuthResponse(state: string, idToken: string, accessToken: string): void {
    const localState = this.storageSvc.getObject(STATE_NAME);
    const idPayload  = this.tokenSvc.decode<IDTokenType>(idToken);
    const nonce      = idPayload.nonce;
    const localNonce = this.storageSvc.getObject(NONCE_NAME);

    // Remove locally stored security information.
    this.storageSvc.removeObject(STATE_NAME);
    this.storageSvc.removeObject(NONCE_NAME);

    // Verify that the state matches up.
    if (!localState || state !== localState)
      throw new Error('AuthService.handleAuthResponse: Invalid state.');

    // Verify the nonce matches up (replay/cut-and-paste URL attack
    // prevention).
    if (!localNonce || nonce !== sha256(localNonce))
      throw new Error('AuthService.handleAuthResponse: Invalid nonce.');

    // Both tokens are stored in memory only (per NIST, the tokens should not
    // be stored in local storage).  If the user refreshes their browser,
    // they'll need to re-auth (silently is an option).
    this.tokenStore.store(idToken, accessToken);
  }

  /**
   * Log out locally and from the auth server.
   */
  logout(): void {
    this.tokenStore.clear();

    const logoutURI = `${this.authConfig.authEndpoint}/api/logout?state=logout` +
      '&post_logout_redirect_uri=' +
      encodeURIComponent(this.authConfig.postLogoutRedirectURI);

    this.urlSvc.go(logoutURI);
  }

  /**
   * Check if this is an auth redirect (e.g. the user is on the redirect page
   * and there are hash parameters).
   */
  authenticating(): boolean {
    return this.urlSvc.getAbsURL().indexOf(this.authConfig.redirectURI) === 0 && !!this.urlSvc.getFragment();
  }

  /**
   * Check if the user is authenticated.
   */
  isAuthenticated(): boolean {
    const accessToken = this.tokenStore.getAccessToken();

    return accessToken !== null && !this.tokenSvc.isExpired(accessToken);
  }
}

