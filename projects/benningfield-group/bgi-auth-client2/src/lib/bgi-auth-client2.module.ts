import { NgModule, ModuleWithProviders } from '@angular/core';

import { AuthConfig } from './auth.config';

@NgModule({
  imports: [],
  declarations: [],
  exports: []
})
export class BgiAuthClient2Module {
  static forRoot(authConfig: AuthConfig): ModuleWithProviders {
    return {
      ngModule: BgiAuthClient2Module,
      providers: [
        {
          provide: 'authConfig',
          useValue: authConfig
        }
      ]
    };
  }
}

