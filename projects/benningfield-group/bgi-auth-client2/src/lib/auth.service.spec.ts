import { TestBed } from '@angular/core/testing';

import { sha256 } from 'js-sha256';

import { StorageService, URLService } from '@benningfield-group/bgi2';

import { AuthService } from './auth.service';
import { AuthConfig } from './auth.config';
import { TokenStore } from './token-store.service';

describe('AuthService', () => {
  let authSvc: AuthService;
  let authConfig: AuthConfig;
  let mockStorageSvc: jasmine.SpyObj<StorageService>;
  let urlSvc: URLService;
  let tokenStore: TokenStore;

  beforeEach(() => {
    authConfig = {
      clientID: 'bgi-auth-client2-demo',
      redirectURI: 'http://localhost:4200/auth',
      authEndpoint: 'https://auth.benningfieldgroup.com',
      postLogoutRedirectURI: 'http://localhost:4200/'
    };

    mockStorageSvc = jasmine
      .createSpyObj('StorageService', ['setObject', 'removeObject', 'getObject']);

    TestBed.configureTestingModule({
      providers: [
        {provide: 'authConfig', useValue: authConfig},
        {provide: StorageService, useValue: mockStorageSvc},
        AuthService
      ]
    });

    urlSvc = TestBed.get(URLService);
    tokenStore = TestBed.get(TokenStore);

    spyOn(urlSvc, 'go');
    spyOn(urlSvc, 'getAbsURL');
    spyOn(urlSvc, 'getFragment');

    authSvc = TestBed.get(AuthService);
  });

  describe('.buildAuthURI()', () => {
    it('creates the auth URI and returns it.', () => {
      const authURI  = authSvc.buildAuthURI();
      const endpoint = authURI.split('?')[0];
      const params   = new Map<string, string>(decodeURI(authURI)
        .split('?')[1]
        .split('&')
        .map(param => param.split('=') as [string, string]));

      expect(endpoint).toBe('https://auth.benningfieldgroup.com/api/authorize');
      expect(params.get('scope')).toBe('openid');
      expect(params.get('response_type')).toBe('id_token token');
      expect(params.get('client_id')).toBe('bgi-auth-client2-demo');
    });

    it('creates and stores a nonce and state.', () => {
      authSvc.buildAuthURI();

      expect(mockStorageSvc.setObject.calls.count()).toBe(2);
      expect(mockStorageSvc.setObject.calls.argsFor(0)[0]).toBe('bgi-auth:nonce');
      expect(mockStorageSvc.setObject.calls.argsFor(1)[0]).toBe('bgi-auth:state');
    });
  });

  describe('.authenticate()', () => {
    it('sends the user to the auth endpoint.', () => {
      authSvc.authenticate();

      expect((urlSvc.go as jasmine.Spy).calls.count()).toBe(1);
      expect((urlSvc.go as jasmine.Spy).calls.argsFor(0)[0].indexOf(authConfig.authEndpoint)).toBe(0);
    });
  });

  describe('.handleAuthRedirect()', () => {
    let state, nonce, nonceHash, jwt, fragment;

    beforeEach(() => {
      state     = 'FAKE_STATE';
      nonce     = 'FAKE_NONCE';
      nonceHash = sha256(nonce);
      jwt       = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3OD' +
        'kwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJub25jZSI6IjBjMTU' +
        'yZGMwMjBiZjU3NTA1OGExZjg1ZDMyODFhOGIyMjE0Y2Y1NjU4ZjIwODM4MGRjZDcwZmI1' +
        'YjIxZjllYjYifQ.ApZGVwqdJ8M8WRrKH6VCS3wJaZe28zMfzl5q7i2NA8o';
      fragment  = `token_type=bearer&state=${state}&id_token=${jwt}&token=${jwt}`;

      (urlSvc.getAbsURL as jasmine.Spy).and.returnValue(`http://localhost:4200/auth#${fragment}`);
      (urlSvc.getFragment as jasmine.Spy).and.returnValue(fragment);

      mockStorageSvc.getObject.and.callFake(key =>
        key === 'bgi-auth:nonce' ? nonce : state);
    });

    it('throws an error if called when authentication is not in progress.', () => {
      (urlSvc.getAbsURL as jasmine.Spy).and.returnValue('https://not-the-auth.page');

      try {
        authSvc.handleAuthRedirect();
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:handleAuthRedirect Authentication not in progress.');
      }
    });

    it('throws an error if the state in storage doesn\'t match the state in the fragement.', () => {
      mockStorageSvc.getObject.and.callFake(key =>
        key === 'bgi-auth:nonce' ? nonce : 'WRONG');

      try {
        authSvc.handleAuthRedirect();
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService.handleAuthResponse: Invalid state.');
      }
    });

    it('throws an error if the nonce in storage doesn\'t match the nonce in the fragement.', () => {
      mockStorageSvc.getObject.and.callFake(key =>
        key === 'bgi-auth:nonce' ? 'WRONG' : state);

      try {
        authSvc.handleAuthRedirect();
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService.handleAuthResponse: Invalid nonce.');
      }
    });

    it('removes the nonce and state from local storage on successful auth.', () => {
      authSvc.handleAuthRedirect();

      expect(mockStorageSvc.removeObject.calls.count()).toBe(2);
    });

    it('stores the access token and id token in the tokenStore.', () => {
      const storeSpy = spyOn(tokenStore, 'store')
        .and.callThrough();

      authSvc.handleAuthRedirect();
      expect(storeSpy).toHaveBeenCalledWith(jwt, jwt);
    });
  });

  describe('.authenticateSilently()', () => {
    beforeEach(() => {
      // An iframe is used to do the auth back-and-forth.  This spoofs the auth URL.
      spyOn(authSvc, 'buildAuthURI').and.returnValue('fake.html');
    });

    afterEach(() => {
      const iframe = document.getElementById('bgiAuthIframe');
      iframe.remove();
    });

    it('creates a hidden iframe.', () => {
      authSvc.authenticateSilently();

      const iframe = document.getElementById('bgiAuthIframe');

      expect(iframe).not.toBeNull();
      expect((iframe as any).src.match(/fake.html$/)).not.toBeNull();
    });
  });

  describe('.handleAuthWebMessage()', () => {
    it('throws an error if no web message is received.', () => {
      try {
        authSvc.handleAuthWebMessage({} as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently No message data received in authorization message.');
      }
    });

    it('throws an error if the response type is not "authorization_response."', () => {
      try {
        authSvc.handleAuthWebMessage({data: {type: 'fake-type'}} as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently Invalid authorization response type: "fake-type"');
      }
    });

    it('throws an error if the "response" is not present.', () => {
      try {
        authSvc.handleAuthWebMessage({data: {type: 'authorization_response'}} as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently Missing "response" in authorization message.');
      }
    });

    it('throws the error returned from the auth server, unmodified.', () => {
      try {
        authSvc.handleAuthWebMessage({
          data: {
            type: 'authorization_response',
            response: {
              error: 'some_auth_error'
            }
          }
        } as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('some_auth_error');
      }
    });

    it('throws an error if the state is not present in the response.', () => {
      try {
        authSvc.handleAuthWebMessage({
          data: {
            type: 'authorization_response',
            response: {
            }
          }
        } as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently Missing "state" in authorization message response.');
      }
    });

    it('throws an error if the id_token is not present in the response.', () => {
      try {
        authSvc.handleAuthWebMessage({
          data: {
            type: 'authorization_response',
            response: {
              state: 'fake state'
            }
          }
        } as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently Missing "id_token" in authorization message response.');
      }
    });

    it('throws an error if the token is not present in the response.', () => {
      try {
        authSvc.handleAuthWebMessage({
          data: {
            type: 'authorization_response',
            response: {
              state: 'fake state',
              id_token: 'fake token'
            }
          }
        } as MessageEvent);
        expect(true).toBe(false);
      }
      catch (err) {
        expect(err.message).toBe('AuthService:authenticateSilently Missing "token" in authorization message response.');
      }
    });
  });

  describe('.logout()', () => {
    it('clears the tokens from storage.', () => {
      const clearSpy = spyOn(tokenStore, 'clear')
        .and.callThrough();

      authSvc.logout();
      expect(clearSpy).toHaveBeenCalled();
    });

    it('sends the user to the logout endpoint.', () => {
      authSvc.logout();

      expect(decodeURIComponent((urlSvc.go as jasmine.Spy).calls.argsFor(0)[0]))
        .toBe('https://auth.benningfieldgroup.com/api/logout?state=logout&post_logout_redirect_uri=http://localhost:4200/');
    });
  });
});

