export class TokenType {
  iat: number;
  exp: number;
  aud: string[];
  iss: string;
  sub: string;
  jti: string;
}

