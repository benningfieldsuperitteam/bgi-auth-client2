export class IDTokenType {
  iat: number;
  exp: number;
  aud: string[];
  iss: string;
  sub: string;
  jti: string;
  nonce: string;
  email: string;
}

