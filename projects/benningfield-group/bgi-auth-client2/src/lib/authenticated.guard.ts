import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {
  constructor(
    private authSvc: AuthService) {
  }

  async canActivate(): Promise<boolean> {
    if (!this.authSvc.authenticating() && !this.authSvc.isAuthenticated()) {
      try {
        await this.authSvc
          .authenticateSilently();
      }
      catch (err) {
        // There should not be any error other than login_required here, but if
        // there is an error it's logged to the console.
        if (err.message !== 'login_required')
          console.error(err);
      }

      if (!this.authSvc.isAuthenticated()) {
        this.authSvc.authenticate();
        return false;
      }
    }

    return this.authSvc.isAuthenticated();
  }
}
