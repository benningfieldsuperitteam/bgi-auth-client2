import { Injectable } from '@angular/core';

import * as jwtDecode_ from 'jwt-decode';

import { TokenType } from './token.type';

const jwtDecode = jwtDecode_;

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  /**
   * Decode a token.
   */
  decode<T extends TokenType>(jwt: string): T {
    try {
      return jwtDecode(jwt);
    }
    catch (e) {
      throw new Error(`TokenService:decode Failed to decode token: ${e.message}`);
    }
  }

  /**
   * Get the expiration as a date.
   */
  getExpiration(jwt: string): Date {
    return new Date(this.decode(jwt).exp * 1000);
  }

  /**
   * Check if the token is expired.
   */
  isExpired(jwt: string): boolean {
    return this.getExpiration(jwt) < new Date();
  }
}

