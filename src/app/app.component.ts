import { Component } from '@angular/core';

import { AuthService } from '@benningfield-group/bgi-auth-client2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent {
  constructor(private authSvc: AuthService) {
  }

  authenticate(): void {
    this.authSvc
      .authenticate();
  }

  authenticateSilently(): void {
    this.authSvc
      .authenticateSilently()
      .catch(err => {
        if (err.message !== 'login_required')
          return Promise.reject(err);
      })
      .then(() =>
        console.log(`Done with silent authentication attempt.  Logged in: ${this.authSvc.isAuthenticated()}`))
      .catch(console.error);
  }

  isAuthenticated(): boolean {
    return this.authSvc
      .isAuthenticated();
  }

  logout(): void {
    this.authSvc
      .logout();
  }
}

