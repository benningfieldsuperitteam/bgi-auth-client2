import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { BgiAuthClient2Module } from '@benningfield-group/bgi-auth-client2';
import { AuthConfig } from '@benningfield-group/bgi-auth-client2';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './home/home.component';

/*const authConfig: AuthConfig = {
  clientID: 'bgi-auth-client2-demo',
  redirectURI: 'http://localhost:4200/auth',
  authEndpoint: 'https://auth.benningfieldgroup.com',
  postLogoutRedirectURI: 'http://localhost:4200/',
};*/
const authConfig: AuthConfig = {
  clientID: 'bgi-auth-client2-demo',
  redirectURI: 'http://localhost:4200/auth',
  authEndpoint: 'http://localhost:3000',
  postLogoutRedirectURI: 'http://localhost:4200/',
};

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    HomeComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BgiAuthClient2Module
      .forRoot(authConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

