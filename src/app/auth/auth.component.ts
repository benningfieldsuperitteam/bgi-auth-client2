import { Component, OnInit } from '@angular/core';

import { AuthService } from '@benningfield-group/bgi-auth-client2';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: []
})
export class AuthComponent implements OnInit {
  constructor(private authSvc: AuthService) {
  }

  ngOnInit(): void {
    if (this.authSvc.authenticating()) {
      try {
        this.authSvc
          .handleAuthRedirect();
      }
      catch (err) {
        if (err.message !== 'login_required')
          console.error(err);
      }
    }
  }
}

